const input = document.querySelector('.area__search'),
      btn = document.querySelector('.area__btn'),
      contentList = document.querySelector('.container-list');
      
const createItem = (city, country, temp, cord, windSpeed, clouds) => {

    const itemCard = document.createElement('div');
    itemCard.className = 'container-list__item container-item';
        
    itemCard.innerHTML = `<div class="container-item__img">
            <span class="container-item__point"></span>
            </div>

            <div class="container-item__info">
                <span class="location">${city}, ${country}</span>
                <span class=" temperature">temperature: ${Math.floor(temp - 273,15)}\u2103, wind ${windSpeed} m/s, clouds ${clouds} %</span>
                <span class="coords">Geo coords: [lng: ${cord[0]}, lat: ${cord[1]}]</span>
            </div>
            <div class="close"></div>`;

    if(input.value) {
        contentList.appendChild(itemCard);
        itemCard.setAttribute(`data-set`, `${city}`);
    }
    
    if(itemCard.hasChildNodes) {
       itemCard.children[itemCard.children.length - 1].addEventListener('click', () => {
            contentList.removeChild(itemCard);
       });
    }
};

const getIpi = (cityName) => {
    fetch('https://api.openweathermap.org/data/2.5/weather?q='+ cityName +'&appid=a6a730e433155631897b094966ff68ee')
        .then(response => response.json())
        .then(data => {

            createItem(data.name, data.sys.country, data.main.temp, [data.coord.lat, data.coord.lon], data.wind.speed, data.clouds.all);
            console.log(data);
        });
};
    
const getValueInput = () => {
    getIpi(input.value);
};

btn.addEventListener('click', getValueInput);

